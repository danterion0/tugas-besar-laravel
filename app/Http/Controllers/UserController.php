<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function profile()
    {

        // dd('ini halaman profile');
        // $request->session()->flush();
        // dd(Auth::user());
        return view('profile');
    }

    public function index()
    {
        return view('user');
    }
}
