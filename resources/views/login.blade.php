<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laravel || Login </title>
    <link rel="stylesheet" href="{{ asset('/assets/css/bootstrap.min.css') }}">

</head>

<body>
    <div class="container">
        <div class="row justify-content-center mt-5">
            <div class="col-md-4">
                @if (session('status'))
                        <div class=" text-center alert alert-danger">
                            {{ session('message') }}
                        </div>
                    @endif
                <div class="card">
                    <div class="card-header bg-transparent mb-0">
                        <h5 class="text-center"><span class="font-weight-bold text-primary">LOGIN</span></h5>
                    </div>
                    <div class="card-body">
                        <form action="" method="POST">
                            @csrf
                            <div class="form-group">
                                <input type="text" name="username" id="username" class="form-control"
                                    placeholder="Username">
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" id="password" class="form-control"
                                    placeholder="Password">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block">Login</button>
                            </div>

                            <div class="form-group">
                                <p class="text-center">Don't have Account ? <a href="register">Sign Up</a></p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script src="{{ asset('/assets/js/bootstrap.min.js') }}"></script>
</body>

</html>
