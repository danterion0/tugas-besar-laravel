@extends('layouts.mainlayout')

@section('title', 'Dashboard')


@section('content')

<h1>Welcome, {{ Auth::user()->username }}</h1>
   
<div class="row">
   <div class="col-sm-4 grid-margin">
       <div class="card">
           <div class="card-body">
               <h5>Book</h5>
               <div class="row">
                   <div class="col-8 col-sm-12 col-xl-8 my-auto">
                       <div class="d-flex d-sm-block d-md-flex align-items-center">
                           <h2 class="mb-0">{{ $book_count }}</h2>
                       </div>
                   </div>
                   <div class="col-4 col-sm-12 col-xl-4 text-center text-xl-right">
                       <i class="icon-lg mdi mdi-codepen text-primary ml-auto"></i>
                   </div>
               </div>
           </div>
       </div>
   </div>
   <div class="col-sm-4 grid-margin">
       <div class="card">
           <div class="card-body">
               <h5>Categories</h5>
               <div class="row">
                   <div class="col-8 col-sm-12 col-xl-8 my-auto">
                       <div class="d-flex d-sm-block d-md-flex align-items-center">
                           <h2 class="mb-0">{{ $category_count }}</h2>
                       </div>
                   </div>
                   <div class="col-4 col-sm-12 col-xl-4 text-center text-xl-right">
                       <i class="icon-lg mdi mdi-wallet-travel text-danger ml-auto"></i>
                   </div>
               </div>
           </div>
       </div>
   </div>
   <div class="col-sm-4 grid-margin">
       <div class="card">
           <div class="card-body">
               <h5>Users</h5>
               <div class="row">
                   <div class="col-8 col-sm-12 col-xl-8 my-auto">
                       <div class="d-flex d-sm-block d-md-flex align-items-center">
                           <h2 class="mb-0">{{ $user_count }}</h2>
                       </div>
                   </div>
                   <div class="col-4 col-sm-12 col-xl-4 text-center text-xl-right">
                       <i class="icon-lg mdi mdi-monitor text-success ml-auto"></i>
                   </div>
               </div>
           </div>
       </div>
   </div>
</div>
<div class="row ">
   <div class="col-12 grid-margin">
       <div class="card">
           <div class="card-body">
               <h4 class="card-title">#Rent Log</h4>

               <table class="table">
                  <thead>
                     <tr>
                        <th>no.</th>
                        <th>User</th>
                        <th>Book Title</th>
                        <th>Rent Date</th>
                        <th>Return Date</th>
                        <th>Actual Return Date</th>
                        <th>Status</th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <td colspan="7" style="text-align: center">No Data</td>
                     </tr>
                  </tbody>
               </table>
           </div>
       </div>
   </div>

@endsection